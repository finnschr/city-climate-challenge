import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NxWelcomeComponent } from './nx-welcome.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { CommonModule } from '@angular/common';
import { AccordionGroupComponent } from './components/accordion/accordion-group.component';

@NgModule({
  declarations: [
    AppComponent,
    NxWelcomeComponent,
    AccordionComponent,
    AccordionGroupComponent,
  ],
  imports: [BrowserModule, CommonModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
